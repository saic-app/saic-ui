import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import About from './components/About';
import Contact from './components/Contact';
import Home from './components/Home';
import Menu from './components/Menu';
import Dashboard from './dashboard-components/Dashboard';
import Clientes from './dashboard-components/Clientes';
import Productos from './dashboard-components/Productos';
import Proveedores from './dashboard-components/Proveedores';
import EditarEmpresa from './dashboard-components/EditarEmpresa';
import EditarUsuario from './dashboard-components/EditarUsuario';
import EditarCliente from './dashboard-components/EditarCliente';
import EditarProveedor from './dashboard-components/EditarProveedor';
import EditarProducto from './dashboard-components/EditarProducto';
import CrearCliente from './dashboard-components/CrearCliente';
import CrearProveedor from './dashboard-components/CrearProveedor';
import CrearProducto from './dashboard-components/CrearProducto';
import ValDeleteCliente from './dashboard-components/ValDeleteCliente';
import ValDeleteProveedor from './dashboard-components/ValDeleteProveedor';
import ValDeleteProducto from './dashboard-components/ValDeleteProducto';

function App() {
  return (
    <div className='container'>
      <Router>
        <Menu></Menu>
        <Switch>
          <Route exact path='/' component={Home}></Route>
          <Route path='/about' component={About}></Route>
          <Route path='/contact' component={Contact}></Route>
          <Route path='/dashboard' component={Dashboard}></Route>
          <Route path='/clientes' component={Clientes}></Route>
          <Route path='/productos' component={Productos}></Route>
          <Route path='/proveedores' component={Proveedores}></Route>
          <Route path='/edit_empresa' component={EditarEmpresa}></Route> 
          <Route path='/edit_usuario' component={EditarUsuario}></Route>
          <Route path='/edit_cliente' component={EditarCliente}></Route>
          <Route path='/edit_producto' component={EditarProducto}></Route>
          <Route path='/edit_proveedor' component={EditarProveedor}></Route>
          <Route path='/add_cliente' component={CrearCliente}></Route>
          <Route path='/add_proveedor' component={CrearProveedor}></Route>
          <Route path='/add_producto' component={CrearProducto}></Route>
          <Route path='/delete_cliente' component={ValDeleteCliente}></Route>
          <Route path='/delete_proveedor' component={ValDeleteProveedor}></Route>
          <Route path='/delete_producto' component={ValDeleteProducto}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
