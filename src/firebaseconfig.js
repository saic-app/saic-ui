import firebase from 'firebase';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyB6sXZXRDjMkEbJJaUItWAxeiCFYQhl3w4",
    authDomain: "saic-fb.firebaseapp.com",
    projectId: "saic-fb",
    storageBucket: "saic-fb.appspot.com",
    messagingSenderId: "695556428846",
    appId: "1:695556428846:web:aa2290464af3d4b437bbbf",
    measurementId: "G-1QM5FSSNNC"
  };
  // Initialize Firebase
  const fire = firebase.initializeApp(firebaseConfig);
  const auth = fire.auth()

  export {auth}

  // firebase.analytics();