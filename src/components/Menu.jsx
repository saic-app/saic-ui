import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {auth} from '../firebaseconfig';

const Menu = () => {
    
    const [user, setUser] = useState(null)
    
    useEffect(() => {
        auth.onAuthStateChanged( (user) => {
            if( user ) {
                setUser(user.email)
            }
        })
    },[])
    
    return (
        <div>
            <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                <ul className='navbar-nav mr-auto'>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/'>Inicio</Link>
                    </li>

                    {
                        user ?
                        (
                            <li className='nav-item'>
                                <Link className='nav-link' to='/dashboard'>MySpace</Link>
                            </li>
                        )
                        :
                        (
                            <span></span>
                        )
                    }

                </ul>

                <ul className='navbar-nav'>

                    <li className='nav-item'>
                        <Link className='nav-link' to='/about'>Sobre Nosotros</Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/contact'>Contacto</Link>
                    </li>

                </ul>
            </nav>
        </div>
    )
}

export default Menu