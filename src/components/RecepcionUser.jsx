import React from 'react';
import { auth } from '../firebaseconfig';

const RecepcionUser = (props) => {
    
    const {email} = props

    const cerrarSesion = () => {
        auth.signOut()
        window.location.reload()
    }

    return(
        <div className='row'>
            <h5 className='text-center'>Bienvenido. Sesión iniciada con el correo {email} </h5>
            <div className='col'></div>
            <div className='col'>
            <button onClick={cerrarSesion} className='btn btn-primary'>Cerrar sesión</button>
            </div>
            <div className='col'></div>
        </div>
    )
}

export default RecepcionUser