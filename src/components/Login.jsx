import React, { useState } from 'react';
import { auth } from '../firebaseconfig';
import {useHistory} from 'react-router-dom';

const Login = () => {
    const historial = useHistory()
    const [email, setEmail] = useState('')
    const [pass, setPass] = useState('')
    const [msgError, setMsgError] = useState(null)

    const IniciarSesion = (e) => {
        e.preventDefault()
        auth.signInWithEmailAndPassword(email,pass)
        .then( (r) => {
            setMsgError(null)
            historial.push('/dashboard')
        })
        .catch( (err) => {
            if(err.code == 'auth/user-not-found'){
                setMsgError('El usuario o contraseña ingresados no es correcto')
            }
            if(err.code == 'auth/wrong-password'){
                setMsgError('El usuario o contraseña ingresados no es correcto')
            }
        })
    }

    return (
        <div className='row mt-5'>
            <div className='col-3'></div>
            <div className='col-3'></div>
            <div className='col'>
                <h3>Inicio de sesión:</h3>
                <form onSubmit={IniciarSesion} className='form-gruop mt-4'>
                    <input
                        onChange={(e) => setEmail(e.target.value)}
                        className='form-control'
                        placeholder='Correo electrónico'
                        type="email" />
                    <input
                        onChange={(e) => setPass(e.target.value)}
                        className='form-control mt-4'
                        placeholder='Contraseña'
                        type="password" />
                    <input className='btn btn-primary btn-block mt-4'
                        value='Ingresar'
                        type="submit" />
                </form>
                {
                    msgError ?
                    (
                        <div className='mt-2 text-center'>
                            {msgError}
                        </div>
                    )
                    :
                    (
                        <span></span>
                    )
                }
            </div>
        </div>
    )
}

export default Login