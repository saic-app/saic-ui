import React, { useEffect, useState } from 'react';
import Login from './Login';
import {auth} from '../firebaseconfig';
import RecepcionUser from './RecepcionUser';


const Home = () => {
    const [user, setUser] = useState(null)
    
    useEffect(() => {
        auth.onAuthStateChanged( (user) => {
            if( user ) {
                setUser(user.email)
            }
        })
    },[])

    return (
        <div>
            <h1>Bienvenido a Home V3</h1>
            <div className='row'>
                <div className='col-5'>

                </div>

                <div className='col-5'>
                    {
                        user ?
                        (
                            <RecepcionUser email={user}></RecepcionUser>
                        )
                        :
                        (
                            <Login></Login>
                        )
                    }
                </div>
            </div>
        </div>
    )
}

export default Home