import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import {proxy} from '../proxyconfig';
import Navmenu from './Navmenu';

const EditarUsuario = (props) => {

    const historial = useHistory()
    const data = props.history.location.state

    const [usuario, setUsuario] = useState(null)
    const [nombre, setNombre] = useState('')
    const [apellidoPaterno, setApellidoPaterno] = useState('')
    const [apellidoMaterno, setApellidoMaterno] = useState('')
    const [correo, setCorreo] = useState('')
    const [telefono, setTelefono] = useState('')
    const [conexion, setConexion] = useState('')

    useEffect(() => {
        getUsuario(data.apiUser.id)
        setConexion(getFecha())
    }, [])

    useEffect(() => {
        setUsuario({
            'id' : data.apiUser.id,
            'nombre': nombre,
            'apellidoPaterno': apellidoPaterno,
            'apellidoMaterno': apellidoMaterno,
            'correo': correo,
            'telefono': telefono,
            'ultimaCon': conexion
        })
    }, [nombre, apellidoMaterno, apellidoPaterno, correo, telefono, conexion])

    const getFecha = () => {
        const tiempo = Date.now()
        const hoy = new Date(tiempo)
        return hoy.toUTCString()
    }

    const getUsuario = async (id) => {
        await axios
            .get('http://'+proxy+'/api/data/user/find/' + id).then(res => {
                setNombre(res.data.nombre)
                setApellidoPaterno(res.data.apellidoPaterno)
                setApellidoMaterno(res.data.apellidoMaterno)
                setCorreo(res.data.correo)
                setTelefono(res.data.telefono)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const guardarUsuario = (e) => {
        e.preventDefault()
        axios.post('http://'+proxy+'/api/data/user/save',usuario).then(res => {
            if(res.data == true){
                historial.push('/dashboard')
            }
        })
    }

    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col-3'></div>
                <div className='col-4'>
                    <h4 className='text-center mt-5'>Editar Usuario</h4>
                    <form onSubmit={guardarUsuario} className='form-group mt-4'>
                        <h6>Nombre</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNombre(e.target.value)}
                            defaultValue={nombre}
                            type="text" />
                        <h6 className='mt-4'>Apellido Paterno</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setApellidoPaterno(e.target.value)}
                            defaultValue={apellidoPaterno}
                            type="text" />
                        <h6 className='mt-4'>Apellido Materno</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setApellidoMaterno(e.target.value)}
                            defaultValue={apellidoMaterno}
                            type="text" />
                        <h6 className='mt-4'>Correo electrónico</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setCorreo(e.target.value)}
                            defaultValue={correo}
                            type="text" />
                        <h6 className='mt-4'>Teléfono/Celular</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setTelefono(e.target.value)}
                            defaultValue={telefono}
                            type="text" />

                        <input className='btn btn-primary btn-block mt-4'
                            value='Guardar Cambios'
                            type="submit" />

                    </form>
                </div>
                <div className='col-3'></div>
            </div>
        </div>
    )
}

export default EditarUsuario