import React from 'react'

const Tabla_encargado = (props) => {

    const { encargado } = props


    return (
        <div className='mb-4'>
            <ul className='list-group'>
                <li className='list-group-item active'> Información de encargado:</li>
                <li className='list-group-item'>Nombre:    {encargado.nombre} {encargado.apellidoPaterno} {encargado.apellidoMaterno} </li>
                <li className='list-group-item'>rut: {encargado.rut} </li>
                <li className='list-group-item'>Contacto: {encargado.numContacto}</li>
            </ul>
        </div>
    )
}

export default Tabla_encargado