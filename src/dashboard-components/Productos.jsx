import React, { useState, useEffect } from 'react';
import { auth } from '../firebaseconfig';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';
import ListaProductos from './ListaProductos';

const Productos = () => {

    const [productos, setProductos] = useState([])
    const [apiUser, setApiUser] = useState(null)

    useEffect(() => {
        auth.onAuthStateChanged((objuser) => {
            if (objuser) {
                getUser(objuser.email)

            }
        })

    }, [])

    const getUser = async (mail) => {
        await axios
            .get('http://' + proxy + '/api/data/user/findbyemail/' + mail).then(res => {
                setApiUser(res.data)
                getProductos(res.data.id)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const getProductos = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/producto/findbyuser/' + id).then(res => {
                setProductos(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    return (
        <div>
            <h3 className='mt-2'>MySpace - Productos</h3>
            <Navmenu></Navmenu>
            <ListaProductos productos={productos} usuario={apiUser}></ListaProductos>
        </div>
    )
}

export default Productos