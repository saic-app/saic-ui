import React from 'react'
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';

const ValDeleteProveedor = (props) => {
    const data = props.history.location.state
    const historial = useHistory()

    const aceptar = () => {
        axios.get('http://' + proxy + '/api/data/proveedor/delete/' +data.selected.id).then(res => {
            if(res.data == true){
                historial.push('/proveedores')
            }
        })
    }

    const cancelar = () => {
        historial.push('/proveedores')
    }

    return (
        <div>
            <Navmenu></Navmenu>
            <h4 className='text-center mt-5'>¿Estas seguro de realizar esta operación? El proceso es irreversible</h4>

            <div className='row mt-5'>
                <div className='col'></div>
                <div className='col'></div>
                <div className='col'>
                    <button onClick={aceptar} className='btn btn-primary'>Aceptar</button>
                </div>
                <div className='col'>
                    <button onClick={cancelar} className='btn btn-danger'>Cancelar</button>
                </div>
                <div className='col'></div>
                <div className='col'></div>
            </div>

        </div>
    )
}

export default ValDeleteProveedor