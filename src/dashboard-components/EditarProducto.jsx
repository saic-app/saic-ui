import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';

const EditarProducto = (props) => {

    const producto = props.location.state.producto
    const historial = useHistory()

    const [newProducto, setNewProducto] = useState(null)
    const usuario = producto.usuario
    const proveedor = producto.proveedor
    const id = producto.id

    const [nombre, setNombre] = useState('')
    const [marca, setMarca] = useState('')
    const [stock, setStock] = useState('')
    const [stockCritico, setStockCritico] = useState('')
    const [precioBase, setPrecioBase] = useState('')

    useEffect(() => {
        setNombre(producto.nombre)
        setMarca(producto.marca)
        setStock(producto.stock)
        setStockCritico(producto.stockCritico)
        setPrecioBase(producto.precioBase)
    }, [])

    useEffect(() => {
        setNewProducto({
            'id': id,
            'nombre': nombre,
            'marca': marca,
            'stock': stock,
            'stockCritico': stockCritico,
            'precioBase': precioBase,
            'esVisible': 1,
            'usuario': usuario,
            'proveedor': proveedor
        })
    }, [nombre, marca, stock, stockCritico, precioBase])

    const guardar = (e) => {
        e.preventDefault()
        axios.post('http://' + proxy + '/api/data/producto/save', newProducto).then(res => {
            if (res.data == true) {
                historial.push('/productos')
            }
        })
    }



    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col-3'></div>
                <div className='col-4'>
                    <h4 className='text-center mt-5'>Editar Producto</h4>
                    <form onSubmit={guardar} className='form-group mt-4'>
                        <h6>Nombre del producto:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNombre(e.target.value)}
                            defaultValue={nombre}
                            type="text" />
                        <h6 className='mt-4'>Marca:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setMarca(e.target.value)}
                            defaultValue={marca}
                            type="text" />
                        <h6 className='mt-4'>Stock:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setStock(e.target.value)}
                            defaultValue={stock}
                            type="number" />
                        <h6 className='mt-4'>Stock Crítico:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setStockCritico(e.target.value)}
                            defaultValue={stockCritico}
                            type="number" />
                        <h6 className='mt-4'>Precio Base:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setPrecioBase(e.target.value)}
                            defaultValue={precioBase}
                            type="text" />
                        <input className='btn btn-primary btn-block mt-4'
                            value='Guardar Cambios'
                            type="submit" />

                    </form>
                </div>
                <div className='col-3'></div>
            </div>

        </div>
    )
}

export default EditarProducto