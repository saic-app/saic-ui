import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import InfoProveedores from './InfoProveedores';

const ListaProveedores = (props) => {

    const { proveedores, usuario } = props

    const historial = useHistory()

    const [selected, setSelected] = useState('')
    const [encargado, setEncargado] = useState('')
    const [domicilio, setDomicilio] = useState('')

    const getEncargado = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/proveedor/getencargado/' + id).then(res => {
                setEncargado(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const getDomicilio = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/proveedor/getdomicilio/' + id).then(res => {
                setDomicilio(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const selectProveedor = (item) => {
        getEncargado(item.id)
        getDomicilio(item.id)
        setSelected(item)
    }

    const agregarProveedor = () => {
        historial.push({
            pathname: '/add_proveedor',
            state: { usuario }
        })
    }


    return (

        <div className='row mt-4'>
            <div className='col-7'>

                <div className='row'>
                    <div className='col'>
                        <h5>Lista de proveedores</h5>
                    </div>
                    <div className='col'>
                        <button onClick={agregarProveedor} className='btn btn-primary float-right'>Agregar Proveedor</button>
                    </div>
                </div>
                <table className='table mt-3'>
                    <thead>
                        <tr className='table-primary'>
                            <th scope='col'>Nombre</th>
                            <th scope='col'>N° Contacto</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            proveedores.map(item =>
                                <tr onClick={() => selectProveedor(item)} key={item.id}>
                                    <th>{item.nombre}</th>
                                    <th>{item.numContacto}</th>
                                </tr>
                            )
                        }
                    </tbody>

                </table>
            </div>

            <div className='col'>
                <InfoProveedores selected={selected} encargado={encargado} domicilio={domicilio} usuario={usuario} ></InfoProveedores>
            </div>
        </div>
    )
}

export default ListaProveedores