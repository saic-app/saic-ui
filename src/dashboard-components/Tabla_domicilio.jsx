import React from 'react'

const Tabla_domicilio = (props) => {

    //el tipo activa el <li> de Comuna para el caso del cliente
    const { domicilio, tipo } = props


    return (
        <div className='mb-4'>
            <ul className='list-group'>
                <li className='list-group-item active'> Domicilio:</li>
                {
                    tipo == 'cliente' ?
                        (
                            <li className='list-group-item'>Comuna: {domicilio.comuna} </li>
                        )
                        :
                        (
                            <span></span>
                        )
                }
                <li className='list-group-item'>Calle: {domicilio.nombre} </li>
                <li className='list-group-item'>Número: {domicilio.numero}</li>
            </ul>
        </div>
    )
}

export default Tabla_domicilio