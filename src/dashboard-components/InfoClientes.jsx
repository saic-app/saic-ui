import React from 'react'
import { useHistory } from 'react-router-dom';
import Tabla_encargado from './Tabla_encargado';
import Tabla_domicilio from './Tabla_domicilio';

const InfoClientes = (props) => {

    const { selected, encargado, domicilio, usuario } = props;

    const historial = useHistory()

    const editarCliente = () => {
        historial.push({
            pathname: '/edit_cliente',
            state: { selected, encargado, domicilio, usuario }
        })
    }

    const eliminar = () => {
        historial.push({
            pathname: '/delete_cliente',
            state: { selected }
        })
    }

    return (
        <div className='col'>
            {
                selected ?
                    (
                        <div>
                            <button onClick={editarCliente} className='btn btn-info'>Editar Cliente</button>
                            <button className='btn btn-success float-right'>Lista de Precios</button>
                            <div className='mt-4'>
                                <Tabla_domicilio domicilio={domicilio} tipo='cliente'></Tabla_domicilio>
                                <Tabla_encargado encargado={encargado}></Tabla_encargado>
                            </div>
                            <button onClick={eliminar} className='btn btn-danger float-right'>Eliminar Cliente</button>
                        </div>
                    )
                    :
                    (
                        <div>
                            <h6 className='text-center'>Selecciona un cliente de la lista para ver detalles.</h6>
                        </div>
                    )
            }
        </div>
    )

}

export default InfoClientes