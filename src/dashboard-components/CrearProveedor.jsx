import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';

const CrearProveedor = (props) => {

    const data = props.history.location.state
    const historial = useHistory()

    const [proveedor, setProveedor] = useState(null)

    const [nombreProveedor, setNombreProveedor] = useState('')
    const [numContacto, setNumContacto] = useState('')

    useEffect(() => {
        setProveedor({
            'nombre': nombreProveedor,
            'numContacto': numContacto,
            'usuario': data.usuario
        })
    }, [nombreProveedor, numContacto])

    const guardar = () => {
        axios.post('http://' + proxy + '/api/data/proveedor/save', proveedor).then(res => {
            if(res.data == true) {
                historial.push('/proveedores')
            }
        })
    }

    const cancelar = () => {
        historial.push('/proveedores')
    }



    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col'></div>
                <div className='col'>
                    <h4 className='text-center mt-5'>Editar Informacion de Proveedor</h4>
                    <form className='form-group mt-4'>
                        <h6>Nombre de proveedor:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNombreProveedor(e.target.value)}
                            defaultValue={nombreProveedor}
                            type="text" />
                        <h6 className='mt-4'>N° contacto:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNumContacto(e.target.value)}
                            defaultValue={numContacto}
                            type="text" />
                    </form>
                </div>
                <div className='col'></div>
            </div>
            <div className='row mb-5 mt-4'>
                <div className='col'></div>
                <div className='col'>
                    <button onClick={guardar} className='btn btn-primary form-control'>Guardar</button>
                </div>
                <div className='col'>
                    <button onClick={cancelar} className='btn btn-danger form-control'>Cancelar</button>
                </div>
                <div className='col'></div>

            </div>
        </div>
    )
}

export default CrearProveedor