import React, { useState, useEffect } from 'react';
import { auth } from '../firebaseconfig';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';
import ListaProveedores from './ListaProveedores';

const Proveedores = () => {

    const [proveedores, setProveedores] = useState([])
    const [apiUser, setApiUser] = useState(null)

    useEffect(() => {
        auth.onAuthStateChanged((objuser) => {
            if (objuser) {
                getUser(objuser.email)

            }
        })

    }, [])

    const getUser = async (mail) => {
        await axios
            .get('http://' + proxy + '/api/data/user/findbyemail/' + mail).then(res => {
                setApiUser(res.data)
                getProveedores(res.data.id)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const getProveedores = async (id) => {
        await axios
            .get('http://'+ proxy + '/api/data/proveedor/findbyuser/'+ id).then(res => {
                setProveedores(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    return (
        <div>
            <h3 className='mt-2'>MySpace - Proveedores</h3>
            <Navmenu></Navmenu>
            <ListaProveedores proveedores={proveedores} usuario={apiUser}></ListaProveedores>
        </div>
    )
}

export default Proveedores