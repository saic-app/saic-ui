import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';

const CrearProducto = (props) => {

    const usuario = props.location.state.usuario
    const historial = useHistory()


    const [newProducto, setNewProducto] = useState(null)
    const [selected, setSelected] = useState({
        'nombre' : ''
    })

    const [proveedores, setProveedores] = useState([])
    const [nombre, setNombre] = useState('')
    const [marca, setMarca] = useState('')
    const [stock, setStock] = useState('')
    const [stockCritico, setStockCritico] = useState('')
    const [precioBase, setPrecioBase] = useState('')

    useEffect(() => {
        getProveedores(usuario.id)
    }, [])

    useEffect(() => {
        setNewProducto({
            'nombre': nombre,
            'marca': marca,
            'stock': stock,
            'stockCritico': stockCritico,
            'precioBase': precioBase,
            'esVisible': 1,
            'usuario': usuario,
            'proveedor': selected
        })
    }, [nombre, marca, stock, stockCritico, precioBase, selected])

    const getProveedores = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/proveedor/findbyuser/' + id).then(res => {
                setProveedores(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const select = (proveedor) => {
        setSelected(proveedor)
    }

    const guardar = () => {
        axios.post('http://' + proxy + '/api/data/producto/savenew', newProducto).then(res => {
            if (res.data == true) {
                historial.push('/productos')
            }
        })
    }

    const cancelar = () => {
        historial.push('/productos')
    }

    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col'>
                    <div className='row'>
                        <div className='col'>
                            <h4 className='text-center mt-5'>Editar Producto</h4>
                            <form className='form-group mt-4'>
                                <h6>Nombre del producto:</h6>
                                <input
                                    className='form-control mt-3'
                                    onChange={(e) => setNombre(e.target.value)}
                                    type="text" />
                                <h6 className='mt-4'>Marca:</h6>
                                <input
                                    className='form-control mt-3'
                                    onChange={(e) => setMarca(e.target.value)}
                                    type="text" />
                                <h6 className='mt-4'>Stock:</h6>
                                <input
                                    className='form-control mt-3'
                                    onChange={(e) => setStock(e.target.value)}
                                    type="number" />
                                <h6 className='mt-4'>Stock Crítico:</h6>
                                <input
                                    className='form-control mt-3'
                                    onChange={(e) => setStockCritico(e.target.value)}
                                    type="number" />
                                <h6 className='mt-4'>Precio Base:</h6>
                                <input
                                    className='form-control mt-3'
                                    onChange={(e) => setPrecioBase(e.target.value)}
                                    type="text" />
                            </form>
                        </div>
                    </div>
                </div>
                <div className='col'>
                    <h4 className='text-center mt-5'>Proveedor</h4>
                    <h5 className='mt-5'>Proveedor seleccionado: </h5>
                    <input
                        className='form-control mt-3'
                        type="text"
                        value={selected.nombre}
                        disabled />
                    <h6 className='text-center'>Para seleccionar un proveedor, haga click en los siguientes de la lista.</h6>
                    <h5 className='text-center mt-5'>Lista de proveedores</h5>
                    <ul className='list-group'>
                        {
                            proveedores.map(item => 
                                <li key={item.id} 
                                className='list-group-item'
                                onClick={() => select(item)}
                                >{item.nombre}</li>    
                            )
                        }
                    </ul>
                </div>
            </div>
            <div className='row mb-5 mt-4'>
                <div className='col'></div>
                <div className='col'>
                    <button onClick={guardar} className='btn btn-primary form-control'>Guardar</button>
                </div>
                <div className='col'>
                    <button onClick={cancelar} className='btn btn-danger form-control'>Cancelar</button>
                </div>
                <div className='col'></div>

            </div>

        </div>
    )
}

export default CrearProducto