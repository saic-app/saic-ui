import React from 'react';
import { useHistory } from 'react-router-dom';

const ListaProductos = (props) => {

    const { productos, usuario } = props

    const historial = useHistory()

    const agregarProducto = () => {
        historial.push({
            pathname: '/add_producto',
            state: { usuario }
        })
    }

    const editar = (producto) => {
        historial.push({
            pathname: '/edit_producto',
            state: { producto }
        })
    }

    const eliminar = (id) => {
        historial.push({
            pathname: '/delete_producto',
            state: { id }
        })
    }

    return (
        <div className='row mt-4'>
            
            <div className='col'>

                <div className='row'>
                    <div className='col'>
                        <h5>Lista de productos</h5>
                    </div>
                    <div className='col'>
                        <button onClick={agregarProducto} className='btn btn-primary float-right'>Agregar Producto</button>
                    </div>
                </div>
                <table className='table mt-3'>
                    <thead>
                        <tr className='table-primary'>
                            <th scope='col'>Nombre</th>
                            <th scope='col'>Marca</th>
                            <th scope='col'>Stock</th>
                            <th scope='col'>Stock Crítico</th>
                            <th scope='col'>Precio Base</th>
                            <th scope='col'>Proveedor</th>
                            <th scope='col'></th>
                            <th scope='col'></th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            productos.map(item =>
                                <tr key={item.id}>
                                    <th>{item.nombre}</th>
                                    <th>{item.marca}</th>
                                    <th>{item.stock}</th>
                                    <th>{item.stockCritico}</th>
                                    <th>{item.precioBase}</th>
                                    <th>{item.proveedor.nombre}</th>
                                    <th>
                                        <button onClick={() => editar(item)} className='btn btn-info'>Editar</button>
                                    </th>
                                    <th>
                                        <button onClick={() => eliminar(item.id)} className='btn btn-danger'>Eliminar</button>
                                    </th>
                                </tr>
                            )
                        }
                    </tbody>

                </table>
            </div>
        </div>
    )
}

export default ListaProductos