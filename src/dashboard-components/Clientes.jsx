import React, { useState, useEffect } from 'react';
import { auth } from '../firebaseconfig';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';
import ListaClientes from './ListaClientes';

const Clientes = () => {

    const [clientes, setClientes] = useState([])
    const [apiUser, setApiUser] = useState(null)

    useEffect(() => {
        auth.onAuthStateChanged((objuser) => {
            if (objuser) {
                getUser(objuser.email)

            }
        })

    }, [])



    const getUser = async (mail) => {
        await axios
            .get('http://' + proxy + '/api/data/user/findbyemail/' + mail).then(res => {
                setApiUser(res.data)
                getClientes(res.data.id)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const getClientes = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/cliente/findbyuser/' + id).then(res => {
                setClientes(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }


    return (
        <div>
            <h3 className='mt-2'>MySpace - Clientes</h3>
            <Navmenu></Navmenu>
            <ListaClientes clientes={clientes} usuario={apiUser}></ListaClientes>
        </div>
    )
}

export default Clientes

