import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';

const EditarDomicilioCliente = (props) => {

    const data = props.history.location.state
    const historial = useHistory()

    // para guardar
    const [cliente, setCliente] = useState(null)
    const [encargado, setEncargado] = useState(null)
    const [domicilio, setDomicilio] = useState(null)


    // para el cliente
    const [idCliente, setIdCliente] = useState('')
    const [nombreCliente, setNombreCliente] = useState('')
    const [numContacto, setNumContacto] = useState('')
    const [giro, setGiro] = useState('')

    //para el domicilio 
    const [idDomicilio, setIdDomicilio] = useState('')
    const [comuna, setComuna] = useState('')
    const [calle, setCalle] = useState('')
    const [numero, setNumero] = useState('')

    // para el encargado
    const [idEncargado, setIdEncargado] = useState('')
    const [rutEncargado, setRutEncargado] = useState('')
    const [nombreEncargado, setNombreEncargado] = useState('')
    const [apellidoPaterno, setApellidoPaterno] = useState('')
    const [apellidoMaterno, setApellidoMaterno] = useState('')
    const [numContactoEncargado, setNumContactoEncargado] = useState('')

    // para el validador
    const [valcliente, setValcliente] = useState(true)
    const [valencargado, setValencargado] = useState(true)
    const [valdomicilio, setValdomicilio] = useState(true)


    useEffect(() => {
        //para el cliente
        setIdCliente(data.selected.id)
        setNombreCliente(data.selected.nombre)
        setNumContacto(data.selected.numContacto)
        setGiro(data.selected.giroNegocio)

        //para el domicilio
        setIdDomicilio(data.domicilio.id)
        setComuna(data.domicilio.comuna)
        setCalle(data.domicilio.calle)
        setNumero(data.domicilio.numero)

        //para el encargado
        setIdEncargado(data.encargado.id)
        setNombreEncargado(data.encargado.nombre)
        setRutEncargado(data.encargado.rut)
        setApellidoMaterno(data.encargado.apellidoMaterno)
        setApellidoPaterno(data.encargado.apellidoPaterno)
        setNumContactoEncargado(data.encargado.numContacto)

        //para validar
        validador()
    }, [])

    useEffect(() => {
        if (valcliente) {
            setCliente({
                'id': idCliente,
                'nombre': nombreCliente,
                'numContacto': numContacto,
                'giroNegocio': giro,
                'user': data.usuario
            })
        } else {
            setCliente({
                'nombre': nombreCliente,
                'numContacto': numContacto,
                'giroNegocio': giro,
                'user': data.usuario
            })
        }
    }, [idCliente, nombreCliente, numContacto, giro])

    useEffect(() => {
        if (valdomicilio) {
            setDomicilio({
                'id': idDomicilio,
                'nombre': calle,
                'numero': numero,
                'comuna': comuna,
                'cliente': cliente
            })
        } else {
            setDomicilio({
                'nombre': calle,
                'numero': numero,
                'comuna': comuna,
                'cliente': cliente
            })
        }
    }, [idDomicilio, calle, numero, comuna, cliente])

    useEffect(() => {
        if (valencargado) {
            setEncargado({
                'id': idEncargado,
                'rut': rutEncargado,
                'nombre': nombreEncargado,
                'apellidoPaterno': apellidoPaterno,
                'apellidoMaterno': apellidoMaterno,
                'numContacto': numContactoEncargado,
                'cliente': cliente
            })
        } else {
            setEncargado({
                'rut': rutEncargado,
                'nombre': nombreEncargado,
                'apellidoPaterno': apellidoPaterno,
                'apellidoMaterno': apellidoMaterno,
                'numContacto': numContactoEncargado,
                'cliente': cliente
            })
        }
    }, [idEncargado, rutEncargado, nombreEncargado, apellidoMaterno, apellidoPaterno, numContactoEncargado, cliente])

    const validador = () => {
        if (data.selected == '') {
            setValcliente(false)
        }
        if (data.domicilio == '') {
            setValdomicilio(false)
        }
        if (data.encargado == '') {
            setValencargado(false)
        }
    }


    const guardar = () => {
        axios.post('http://' + proxy + '/api/data/cliente/save', cliente).then(res => {
            if (res.data == true) {
                axios.post('http://' + proxy + '/api/data/cliente/savedomicilio', domicilio)
                axios.post('http://' + proxy + '/api/data/cliente/saveencargado', encargado)
                historial.push('/clientes')
            }
        })
    }

    const cancelar = () => {
        historial.push('/clientes')
    }

    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col'>
                    <h4 className='text-center mt-5'>Editar Informacion de Cliente</h4>
                    <form className='form-group mt-4'>
                        <h6>Nombre de cliente:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNombreCliente(e.target.value)}
                            defaultValue={nombreCliente}
                            type="text" />
                        <h6 className='mt-4'>N° contacto:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNumContacto(e.target.value)}
                            defaultValue={numContacto}
                            type="text" />
                        <h6 className='mt-4'>Giro de negocio:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setGiro(e.target.value)}
                            defaultValue={giro}
                            type="text" />
                    </form>
                </div>
                <div className='col'>
                    <div className='col'>
                        <h4 className='text-center mt-5'>Editar Domicilio de Cliente</h4>
                        <form className='form-group mt-4'>
                            <h6>Comuna:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setComuna(e.target.value)}
                                defaultValue={comuna}
                                type="text" />
                            <h6 className='mt-4'>Calle:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setCalle(e.target.value)}
                                defaultValue={calle}
                                type="text" />
                            <h6 className='mt-4'>Número:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setNumero(e.target.value)}
                                defaultValue={numero}
                                type="text" />
                        </form>
                    </div>
                    <div className='col'>
                        <h4 className='text-center mt-5'>Editar Encargado de Cliente</h4>
                        <form className='form-group mt-4'>
                            <h6>Nombre de encargado:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setNombreEncargado(e.target.value)}
                                defaultValue={nombreEncargado}
                                type="text" />
                            <h6 className='mt-4'>Apellido paterno de encargado:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setApellidoPaterno(e.target.value)}
                                defaultValue={apellidoPaterno}
                                type="text" />
                            <h6 className='mt-4'>Apellido materno de encargado:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setApellidoMaterno(e.target.value)}
                                defaultValue={apellidoMaterno}
                                type="text" />
                            <h6 className='mt-4'>Rut de encargado:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setRutEncargado(e.target.value)}
                                defaultValue={rutEncargado}
                                type="text" />
                            <h6 className='mt-4'>N° contacto encargado:</h6>
                            <input
                                className='form-control mt-3'
                                onChange={(e) => setNumContactoEncargado(e.target.value)}
                                defaultValue={numContactoEncargado}
                                type="text" />
                        </form>
                    </div>

                </div>
            </div>
            <div className='row mb-5 mt-4'>
                <div className='col'></div>
                <div className='col'>
                    <button onClick={guardar} className='btn btn-primary form-control'>Guardar</button>
                </div>
                <div className='col'>
                    <button onClick={cancelar} className='btn btn-danger form-control'>Cancelar</button>
                </div>
                <div className='col'></div>

            </div>


        </div>
    )
}

export default EditarDomicilioCliente