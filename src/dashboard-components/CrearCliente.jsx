import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import Navmenu from './Navmenu';

const CrearCliente = (props) => {

    const data = props.history.location.state
    const historial = useHistory()

    const [cliente, setCliente] = useState(null)

    const [nombreCliente, setNombreCliente] = useState('')
    const [numContacto, setNumContacto] = useState('')
    const [giro, setGiro] = useState('')

    useEffect(() => {
        setCliente({
            'nombre': nombreCliente,
            'numContacto': numContacto,
            'giroNegocio': giro,
            'user': data.usuario
        })

    }, [nombreCliente, numContacto, giro])

    const guardar = () => {
        axios.post('http://' + proxy + '/api/data/cliente/save', cliente).then(res => {
            if (res.data == true) {
                historial.push('/clientes')
            }
        })
    }

    const cancelar = () => {
        historial.push('/clientes')
    }


    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col'></div>
                <div className='col'>
                    <h4 className='text-center mt-5'>Editar Informacion de Cliente</h4>
                    <form className='form-group mt-4'>
                        <h6>Nombre de cliente:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNombreCliente(e.target.value)}
                            defaultValue={nombreCliente}
                            type="text" />
                        <h6 className='mt-4'>N° contacto:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNumContacto(e.target.value)}
                            defaultValue={numContacto}
                            type="text" />
                        <h6 className='mt-4'>Giro de negocio:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setGiro(e.target.value)}
                            defaultValue={giro}
                            type="text" />
                    </form>
                </div>
                <div className='col'></div>
            </div>
            <div className='row mb-5 mt-4'>
                <div className='col'></div>
                <div className='col'>
                    <button onClick={guardar} className='btn btn-primary form-control'>Guardar</button>
                </div>
                <div className='col'>
                    <button onClick={cancelar} className='btn btn-danger form-control'>Cancelar</button>
                </div>
                <div className='col'></div>

            </div>


        </div>
    )
}

export default CrearCliente