import React from 'react'
import { useHistory } from 'react-router-dom';
import Tabla_encargado from './Tabla_encargado';
import Tabla_domicilio from './Tabla_domicilio';

const InfoProveedores = (props) => {
    
    const { selected, encargado, domicilio, usuario } = props;

    const historial = useHistory()

    const editarProveedor = () => {
        historial.push({
            pathname: '/edit_proveedor',
            state: { selected, encargado, domicilio, usuario }
        })
    }

    const eliminar = () => {
        historial.push({
            pathname: '/delete_proveedor',
            state: { selected }
        })
    }

    return(
        <div className='col'>
            {
                selected ?
                    (
                        <div>
                            <button onClick={editarProveedor} className='btn btn-info'>Editar Proveedor</button>
                            <div className='mt-4'>
                                <Tabla_domicilio domicilio={domicilio} tipo='proveedor'></Tabla_domicilio>
                                <Tabla_encargado encargado={encargado}></Tabla_encargado>
                            </div>
                            <button onClick={eliminar} className='btn btn-danger float-right'>Eliminar Proveedor</button>
                        </div>
                    )
                    :
                    (
                        <div>
                            <h6 className='text-center'>Selecciona un proveedor de la lista para ver detalles.</h6>
                        </div>
                    )
            }
        </div>
    )
}

export default InfoProveedores