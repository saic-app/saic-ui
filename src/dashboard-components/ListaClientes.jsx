import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { proxy } from '../proxyconfig';
import InfoClientes from './InfoClientes';

const ListaClientes = (props) => {

    const { clientes, usuario } = props

    const historial = useHistory()

    const [selected, setSelected] = useState('')
    const [encargado, setEncargado] = useState('')
    const [domicilio, setDomicilio] = useState('')

    const getEncargado = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/cliente/getencargado/' + id).then(res => {
                setEncargado(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const getDomicilio = async (id) => {
        await axios
            .get('http://' + proxy + '/api/data/cliente/getdomicilio/' + id).then(res => {
                setDomicilio(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const selectCliente = (item) => {
        getEncargado(item.id)
        getDomicilio(item.id)
        setSelected(item)
    }

    const agregarCliente = () => {
        historial.push({
            pathname: '/add_cliente',
            state: { usuario }
        })
    }

    return (

        <div className='row mt-4'>
            <div className='col-7'>

                <div className='row'>
                    <div className='col'>
                        <h5>Lista de clientes</h5>
                    </div>
                    <div className='col'>
                        <button onClick={agregarCliente} className='btn btn-primary float-right'>Agregar Cliente</button>
                    </div>
                </div>
                <table className='table mt-3'>
                    <thead>
                        <tr className='table-primary'>
                            <th scope='col'>Nombre</th>
                            <th scope='col'>N° Contacto</th>
                            <th scope='col'>Giro Negocio</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            clientes.map(item =>
                                <tr onClick={() => selectCliente(item)} key={item.id}>
                                    <th>{item.nombre}</th>
                                    <th>{item.numContacto}</th>
                                    <th>{item.giroNegocio}</th>
                                </tr>
                            )
                        }
                    </tbody>

                </table>
            </div>

            <div className='col'>
                <InfoClientes selected={selected} encargado={encargado} domicilio={domicilio} usuario={usuario}></InfoClientes>
            </div>
        </div>


    )
}

export default ListaClientes