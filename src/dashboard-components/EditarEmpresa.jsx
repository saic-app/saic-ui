import React, { useState, useEffect } from 'react';
import {useHistory} from 'react-router-dom';
import axios from 'axios';
import {proxy} from '../proxyconfig';
import Navmenu from './Navmenu';

const EditarEmpresa = (props) => {

    const data = props.history.location.state
    const historial = useHistory()

    const [empresa, setEmpresa] = useState(null)
    const [nombre, setNombre] = useState('')
    const [rut, setRut] = useState('')
    const [giro, setGiro] = useState('')
    const [idEmpresa,setIdEmpresa] = useState('')
    

    useEffect(() => {
        getEmpresa(data.apiUser.id)

    }, [])

    useEffect(() => {
        setEmpresa({
            'id' : idEmpresa,
            'nombre': nombre,
            'rut': rut,
            'giroNegocio': giro,
            'usuario': data.apiUser
        })
        
    }, [nombre,rut,giro])


    const getEmpresa = async (id) => {
        await axios
            .get('http://'+proxy+'/api/data/user/getempresa/' + id).then(res => {
                setIdEmpresa(res.data.id)
                setNombre(res.data.nombre)
                setRut(res.data.rut)
                setGiro(res.data.giroNegocio)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const guardarEmpresa = (e) => {
        e.preventDefault()
        axios.post('http://'+proxy+'/api/data/user/saveempresa', empresa).then(res => {
            if(res.data == true){
                historial.push('/dashboard')
            }
        })
    }

    
    return (
        <div>
            <Navmenu></Navmenu>
            <div className='row'>
                <div className='col-3'></div>
                <div className='col-4'>
                    <h4 className='text-center mt-5'>Editar Empresa</h4>
                    <form onSubmit={guardarEmpresa} className='form-group mt-4'>
                        <h6>Nombre de empresa:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setNombre(e.target.value)}
                            defaultValue={nombre}
                            type="text" />
                        <h6 className='mt-4'>Rut:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setRut(e.target.value)}
                            defaultValue={rut}
                            type="text" />
                        <h6 className='mt-4'>Giro de negocio:</h6>
                        <input
                            className='form-control mt-3'
                            onChange={(e) => setGiro(e.target.value)}
                            defaultValue={giro}
                            type="text" />

                        <input className='btn btn-primary btn-block mt-4'
                            value='Guardar Cambios'
                            type="submit" />

                    </form>
                </div>
                <div className='col-3'></div>
            </div>

        </div>
    )
}

export default EditarEmpresa