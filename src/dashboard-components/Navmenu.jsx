import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { auth } from '../firebaseconfig';


const Navmenu = () => {

    const historial = useHistory()

    const cerrarSesion = () => {
        auth.signOut()
        historial.push('/')
        window.location.reload()
    }

    return (
        <div>
            <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                <ul className='navbar-nav mr-auto'>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/dashboard'>MySpace</Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/clientes'>Clientes</Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/proveedores'>Proveedores</Link>
                    </li>
                    <li className='nav-item'>
                        <Link className='nav-link' to='/productos'>Productos</Link>
                    </li>
                </ul>

                <ul className="navbar-nav float-right">
                    <li className='nav-item'>
                        <Link className='nav-link' onClick={cerrarSesion} to=''>Cerrar sesión</Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Navmenu