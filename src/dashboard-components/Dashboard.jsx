import React, { useEffect, useState } from 'react';
import { auth } from '../firebaseconfig';
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import {proxy} from '../proxyconfig';
import Navmenu from './Navmenu';


const Dashboard = () => {
    const historial = useHistory()
    const [user, setUser] = useState(null)
    const [apiUser, setApiUser] = useState({
        nombre: '',
        apellidoPaterno: '',
        apellidoMaterno: ''
    })

    const [apiEmpresa, setApiEmpresa] = useState({
        nombre: 'No registrada',
        rut: 'No registrada',
        giroNegocio: 'No registrada'

    })


    useEffect(() => {
        auth.onAuthStateChanged((objuser) => {
            if (objuser) {
                setUser(objuser.email)
                getUser(objuser.email)
                
            }
        })

    }, [])

    const getUser = async (mail) => {
        await axios
            .get('http://'+proxy+'/api/data/user/findbyemail/' + mail).then(res => {
                setApiUser(res.data)
                getEmpresa(res.data.id)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const getEmpresa = async (id) => {
        await axios
            .get('http://'+proxy+'/api/data/user/getempresa/' + id).then(res => {
                setApiEmpresa(res.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    const editarUsuario = () => {
        historial.push({
            pathname: '/edit_usuario',
            state: {apiUser}
        })
    }

    const editarEmpresa = () => {
        historial.push({
            pathname: '/edit_empresa',
            state: {apiUser}
        })
    }


    return (
        <div>
            <h3 className='mt-2'>MySpace</h3>
            <Navmenu></Navmenu>
            <h5>Bienvenido {apiUser.nombre} {apiUser.apellidoPaterno} {apiUser.apellidoMaterno} </h5>

            <div className='row mt-3'>
                <div className='col'>
                    <ul className='list-group'>
                        <li className='list-group-item active'> Información de usuario:</li>
                        <li className='list-group-item'>Ultima conexión:    {apiUser.ultimaCon} </li>
                        <li className='list-group-item'>Correo electrónico: {apiUser.correo} </li>
                        <li className='list-group-item'>Telefono: {apiUser.telefono}</li>
                        <button className='btn btn-info float-right' onClick={editarUsuario}>Editar Usuario</button>
                    </ul>
                </div>

                <div className='col'>
                    <ul className='list-group'>
                        <li className='list-group-item active'> Información de la empresa:</li>
                        <li className='list-group-item'>Nombre de empresa:    {apiEmpresa.nombre}  </li>
                        <li className='list-group-item'>Rut: {apiEmpresa.rut}</li>
                        <li className='list-group-item'>Giro de negocio: {apiEmpresa.giroNegocio}</li>
                        <button className='btn btn-info float-right' onClick={editarEmpresa}>Editar Empresa</button>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Dashboard